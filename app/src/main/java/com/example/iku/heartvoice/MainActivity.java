package com.example.iku.heartvoice;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.HashMap;


public class MainActivity extends ActionBarActivity {
    public static RequestQueue mQueue;
    public static LocationManager locationManager = null;
    public static Location location = null;
    public final static String url = "http://wiki.xxxc.at/ci/familiarjson";

    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
        //当坐标改变时触发此函数，如果Provider传进相同的坐标，它就不会被触发
        // log it when the location changes
            if (location != null) {
                Log.i("SuperMap", "Location changed : Lat: " + location.getLatitude() + " Lng: " + location.getLongitude());
            }
        }

        public void onProviderDisabled(String provider) {
        // Provider被disable时触发此函数，比如GPS被关闭
        }

        public void onProviderEnabled(String provider) {
        // Provider被enable时触发此函数，比如GPS被打开
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        // Provider的转态在可用、暂时不可用和无服务三个状态直接切换时触发此函数
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        locationManager = (LocationManager) MainActivity.this.getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, locationListener);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void inputAct(View view) {
        Intent intent = new Intent(this, InputActivity.class);
        startActivity(intent);
    }


    public void showText(JSONObject response) throws JSONException {
        int tempCount=0;
        while(tempCount<2) {
            int resId = getResources().getIdentifier("textView" + tempCount, "id", getPackageName());
            TextView text = (TextView) findViewById(resId);
            text.setText(response.getJSONArray("result").getJSONObject(tempCount).getString("text"));
            tempCount++;
        }
    }
    public int getMessage(View view) {

        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        Log.d("TAG",String.valueOf(location.getLatitude()));
        Log.d("TAG",String.valueOf(location.getLongitude()));
        //set parameters
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("hv_act", "1");
        params.put("hv_lat", String.valueOf(location.getLatitude()));
        params.put("hv_lng", String.valueOf(location.getLongitude()));

        JsonObjectRequest req = new JsonObjectRequest(url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("Response:%n %s", response.toString(4));
                            JSONArray nList = response.getJSONArray("result");//获取JSONArray
                            int length = nList.length();
                            for(int i = 0; i < length; i++){//遍历JSONArray
                                Log.d("debugTest",Integer.toString(i));
                                JSONObject oj = nList.getJSONObject(i);
                                int j=i+1;
                                String textID = "textView" + j;
                               int resId = getResources().getIdentifier(textID, "id", getPackageName());
                                TextView text = (TextView) findViewById(resId);
                                text.setText(oj.getString("text"));
                                //text.setText("test");
                            }

                            //showText(response);
                            String text = response.getJSONArray("result").getJSONObject(0).getString("text");
                            Log.d("Text", text);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error: ", error.getMessage());
            }
        });
        mQueue = Volley.newRequestQueue(this);
        mQueue.add(req);



        return 0;
    }
}
