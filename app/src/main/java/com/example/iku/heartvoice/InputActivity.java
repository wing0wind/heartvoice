package com.example.iku.heartvoice;

import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class InputActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_input, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public int sendMessage(View view) {

        MainActivity.location = MainActivity.locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        //set parameters
        EditText editText = (EditText) findViewById(R.id.message);
        String message = editText.getText().toString();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("hv_act", "50");
        params.put("hv_lat", String.valueOf(MainActivity.location.getLatitude()));
        params.put("hv_lng", String.valueOf(MainActivity.location.getLongitude()));
        params.put("text", message);
        params.put("name", "unknown");

        JsonObjectRequest req = new JsonObjectRequest(MainActivity.url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("Response:%n %s", response.toString(4));
                            Intent intent = new Intent(InputActivity.this, MainActivity.class);
                            startActivity(intent);
                            //String text = response.getJSONArray("result").getJSONObject(0).getString("text");
                            //Log.d("Text", text);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error: ", error.getMessage());
            }
        });
        MainActivity.mQueue = Volley.newRequestQueue(this);
        MainActivity.mQueue.add(req);
        return 0;
    }
}
